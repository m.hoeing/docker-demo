FROM debian:11

RUN apt-get update && \
    apt-get install -y build-essential cmake git libjson-c-dev libwebsockets-dev

WORKDIR /app

RUN git clone https://github.com/tsl0922/ttyd.git

WORKDIR /app/ttyd/build

RUN cmake .. && \
    make && \
    make install

RUN useradd --create-home --shell /bin/bash nonroot

USER nonroot

EXPOSE 7681

ENTRYPOINT ["ttyd", "bash"]
