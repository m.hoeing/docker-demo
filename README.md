[TOC]

# geändert

# ttyd Docker Image

This Docker image contains ttyd, a tool for sharing terminals over the web, and Prolog.

## Requirements

- Docker so soll es sein mit Sternchen *

## Usage

1. Clone the repository:

```bash
git clone https://github.com/[YOUR_REPO].git
```

2. Change into the repository directory:

```bash
cd [YOUR_REPO]
```

3. Build the Docker image:

```bash
docker build -t bash_ttyd .
```

4. Start the container:

```bash
docker run -p 7681:7681 --name bash_ttyd -d bash_ttyd
```

5. Install at hub

```bash
docker tag bash_ttyd biederbe/bash_ttyd:latest

docker login

docker push biederbe/bash_ttyd:latest
```

6. Run at kubernetes

```bash
helm install mybash -n bash_ttyd
```

7. Remove 

```bash
helm delete mybash
```
